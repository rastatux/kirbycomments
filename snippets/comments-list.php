<?php

/*
 * This is the example comments list snippet. Feel free to use this code as a
 * reference for creating your own, custom comments snippet.
 * 
 * Custom snippet markup guide:
 * <https://github.com/Addpixel/KirbyComments#custom-markup>
 * 
 * API documentation:
 * <https://github.com/Addpixel/KirbyComments#api-documentation>
 */

$comments = $page->comments();
$status = $comments->process();

?>
<?php if (!$comments->isEmpty()): ?>
	<h2><?= t('kirbycomments.comments') ?></h2>
	
	<?php foreach ($comments as $comment): ?>
		<article id="comment-<?php echo $comment->id() ?>" class="comment<?php e($comment->isPreview(), ' preview"') ?>">
			<h3>
				<?php e($comment->isLinkable(), '<a rel="nofollow noopener" href="'.$comment->website().'" target="_blank">') ?>
				<?php echo $comment->name() ?>
				<?php e($comment->isLinkable(), '</a>') ?>
			</h3>
			
			<aside class="comment-info">
				<?php if ($comment->isPreview()): ?>
					<p><?= t('kirbycomments.preview_text') ?> <a href="#comments-submit"><?= t('kirbycomments.send') ?></a>.</p>
				<?php else: ?>
					<p><?= t('kirbycomments.posted_on') ?> <?php echo $comment->date('Y-m-d') ?>.
						<a href="#comment-<?php echo $comment->id() ?>" title="Permalink" area-label="Permalink">#</a></p>
				<?php endif ?>
			</aside>
			
			<?= $comment->message() ?>
		</article>
	<?php endforeach ?>
<?php endif ?>
